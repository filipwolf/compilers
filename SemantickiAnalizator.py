import fileinput

zastava = False
flag = False
flag2 = False
flag3 = False
flag4 = False
flag5 = False

mapa = {}
mapaPetlja = {}

for line in fileinput.input():
    line = line.lstrip()
    if line.startswith("<naredba_pridruzivanja>"):
        zastava = True
        continue
    if not line.startswith("KR_ZA") and not line.startswith("KR_AZ") and not line.startswith("IDN") and not line.startswith("BROJ") and not line.startswith("KR_OD"):
        continue
    if line.startswith("KR_ZA"):
        flag = True
        flag2 = True
        continue
    if line.startswith("KR_AZ"):
        flag = False
        continue

    str = line.split()
    if flag is False:
        if line.startswith("IDN"):
            if str[2] not in mapa:
                mapa[str[2]] = str[1]
                zastava = False
            elif zastava is False and str[2] in mapa:
                print(str[1] + " " + mapa[str[2]] + " " + str[2])
                zastava = False
            elif zastava is True and str[2] in mapa:
                zastava = False
        if line.startswith("BROJ"):
            zastava = False
    else:
        if flag2 is True:
            mapaPetlja[str[2]] = str[1]
            flag2 = False
        elif line.startswith("KR_OD"):  
            flag3 = True
        elif flag3 is True:
            if line.startswith("IDN"):
                print(str[1] + " " + mapa[str[2]] + " " + str[2])
            flag4 = True
            flag3 = False
        elif flag4 is True:
            if line.startswith("IDN"):
                print(str[1] + " " + mapa[str[2]] + " " + str[2])
            flag4 = False
            flag5 = True
        elif flag5 is True:
            if str[2] not in mapaPetlja and str[2] not in mapa:
                mapaPetlja[str[2]] = str[1]
                zastava = False
            elif str[2] in mapa and zastava is False:
                print(str[1] + " " + mapa[str[2]] + " " + str[2])
            elif (str[2] in mapa or str[2] in mapaPetlja) and zastava is True:
                zastava = False
            elif str[2] in mapaPetlja and zastava is False:
                print(str[1] + " " + mapaPetlja[str[2]] + " " + str[2])



