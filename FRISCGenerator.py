import fileinput

zastava = False
flag = False
flag2 = False
flag3 = False
flag4 = False
flag5 = False
flag6 = False
flag7 = False
ready = False
spremi = False
zadnji = ""
operator = ""
later = ""
nekaj = False
zadnji2 = ""
petljaKraj = ""
operatorPetlja =""
broj = 0

mapa = {}
mapaPetlja = {}
f = open("a.frisc", "x")
f.write(" MOVE 40000, R7\n")

for line in fileinput.input():
    line = line.lstrip()
    if line.startswith("OP_PLUS"):
        operator = "+"
    if line.startswith("OP_MINUS"):
        operator = "-"
    if line.startswith("OP_PUTA"):
        operator = "*"
    if line.startswith("OP_DIJELI"):
        operator = "/"
    if line.startswith("<naredba_pridruzivanja>"):
        spremi = False
        zastava = True
        ready = False
        continue
    elif line.startswith("<E_lista>"):
        ready = True
    elif line.startswith("<lista_naredbi>"):
        operator = ""
        spremi = True
    elif not line.startswith("KR_ZA") and not line.startswith("KR_AZ") and not line.startswith("IDN") and not line.startswith("BROJ") and not line.startswith("KR_OD"):
        continue
    elif line.startswith("KR_ZA"):
        flag = True
        flag2 = True
        continue
    elif line.startswith("KR_AZ"):
        flag = False
        continue

    recenica = line.split()
    if nekaj is True:
        nekaj = False
        later = operator
    if flag is False:
        if ready is True and zadnji is not "":
            if operator == "+":
                f.write(" POP R1\n")
                f.write(" POP R0\n")
                f.write(" ADD R0, R1, R2\n")
                f.write(" PUSH R2\n")
            elif operator == "-":
                f.write(" POP R1\n")
                f.write(" POP R0\n")
                f.write(" SUB R0, R1, R2\n")
                f.write(" PUSH R2\n")
            elif operator == "*":
                f.write(" CALL MUL\n")
            elif operator == "/":
                f.write(" CALL DIV\n")
            else:
                nekaj = True
            if later != operator:
                if later == "+":
                    f.write(" POP R1\n")
                    f.write(" POP R0\n")
                    f.write(" ADD R0, R1, R2\n")
                    f.write(" PUSH R2\n")
                elif later == "-":
                    f.write(" POP R1\n")
                    f.write(" POP R0\n")
                    f.write(" SUB R0, R1, R2\n")
                    f.write(" PUSH R2\n")
                elif later == "*":
                    f.write(" CALL MUL\n")
                elif later == "/":
                    f.write(" CALL DIV\n")

            ready = False
        if spremi is True and zadnji is not "":
            spremi = False
            f.write(" POP R0\n")
            f.write(" STORE R0, (" + zadnji + ")\n")
        if line.startswith("IDN"):
            if recenica[2] not in mapa:
                mapa[recenica[2]] = recenica[1]
                zastava = False
                zadnji = str(recenica[2])
            elif zastava is False and recenica[2] in mapa:
                f.write(" LOAD R0, (" + recenica[2] + ")\n")
                f.write(" PUSH R0\n")
                zastava = False
            elif zastava is True and recenica[2] in mapa:
                zastava = False
        if line.startswith("BROJ"):
            f.write(" MOVE %D " + recenica[2] + ", R0\n")
            f.write(" PUSH R0\n")
            zastava = False
    else:
        if flag2 is True and not line.startswith("<E_lista>") and not line.startswith("<lista_naredbi>"):
            mapaPetlja[recenica[2]] = recenica[1]
            flag2 = False
            zadnji2 = recenica[2]
        elif line.startswith("KR_OD"):
            flag3 = True
        elif flag3 is True and not line.startswith("<E_lista>") and not line.startswith("<lista_naredbi>"):
            if line.startswith("IDN"):
                continue
            if line.startswith("BROJ"):
                f.write(" MOVE %D " + recenica[2] + ", R0\n")
                f.write(" PUSH R0\n")
                f.write(" POP R0\n")
                f.write(" STORE R0, (" + zadnji2 + ")\n")
            flag4 = True
            flag3 = False
        elif flag4 is True and not line.startswith("<E_lista>") and not line.startswith("<lista_naredbi>"):
            if line.startswith("IDN"):
                flag6 = True
                petljaKraj = recenica[2]
            flag4 = False
            flag5 = True
        elif flag6 is True:
            if line.startswith("IDN") and flag7 is False:
                flag6 = False
                flag5 = True

            elif line.startswith("+") or line.startswith("-") or line.startswith("*") or line.startswith("/"):
                operatorPetlja = operator
                flag7 = True
            if flag7 is True and line.startswith("IDN"):
                if operator is "+":
                    broj = petljaKraj + recenica[2]
                elif operator is "-":
                    broj = petljaKraj - recenica[2]
                elif operator is "*":
                    broj = petljaKraj * recenica[2]
                elif operator is "/":
                    broj = petljaKraj / recenica[2]
                flag6 = False
                flag5 = True
        elif flag5 is True:
            if recenica[2] not in mapaPetlja and recenica[2] not in mapa and not line.startswith("<E_lista>") and not line.startswith("<lista_naredbi>"):
                mapaPetlja[recenica[2]] = recenica[1]
                zastava = False
            elif recenica[2] in mapa and zastava is False:
                if ready is True:
                    if operator == "+":
                        f.write(" POP R1")
                        f.write(" POP R0")
                        f.write(" ADD R0, R1, R2")
                        f.write(" PUSH R2")
                    elif operator == "-":
                        f.write(" POP R1")
                        f.write(" POP R0")
                        f.write(" SUB R0, R1, R2")
                        f.write(" PUSH R2")
                    elif operator == "*":
                        f.write(" CALL MUL")
                    elif operator == "/":
                        f.write(" CALL DIV")
                    else:
                        nekaj = True
                    if later != operator:
                        if later == "+":
                            f.write(" POP R1")
                            f.write(" POP R0")
                            f.write(" ADD R0, R1, R2")
                            f.write(" PUSH R2")
                        elif later == "-":
                            f.write(" POP R1")
                            f.write(" POP R0")
                            f.write(" SUB R0, R1, R2")
                            f.write(" PUSH R2")
                        elif later == "*":
                            f.write(" CALL MUL")
                        elif later == "/":
                            f.write(" CALL DIV")
                continue
            elif (recenica[2] in mapa or recenica[2] in mapaPetlja) and zastava is True:
                zastava = False
            elif recenica[2] in mapaPetlja and zastava is False:
                continue


f.write(" LOAD R6, (rez)\n")
f.write(" HALT\n")
f.write("MD_SGN  MOVE 0, R6\n        XOR R0, 0, R0\n        JP_P MD_TST1\n        XOR R0, -1, R0\n        ADD R0, 1, R0\n        MOVE 1, R6\nMD_TST1 XOR R1, 0, R1\n        JP_P MD_SGNR\n        XOR R1, -1, R1\n        ADD R1, 1, R1\n        XOR R6, 1, R6\nMD_SGNR RET\nMD_INIT POP R4\n        POP R3\n        POP R1\n        POP R0\n        CALL MD_SGN\n        MOVE 0, R2\n        PUSH R4\n        RET\nMD_RET  XOR R6, 0, R6\n        JP_Z MD_RET1\n        XOR R2, -1, R2\n        ADD R2, 1, R2\nMD_RET1 POP R4\n        PUSH R2\n        PUSH R3\n        PUSH R4\n        RET\nMUL     CALL MD_INIT\n        XOR R1, 0, R1\n        JP_Z MUL_RET\n        SUB R1, 1, R1\nMUL_1   ADD R2, R0, R2\n        SUB R1, 1, R1\n        JP_NN MUL_1\n")
f.write("MUL_RET CALL MD_RET\n        RET\nDIV     CALL MD_INIT\n        XOR R1, 0, R1\n        JP_Z DIV_RET\nDIV_1   ADD R2, 1, R2\n        SUB R0, R1, R0\n        JP_NN DIV_1\n        SUB R2, 1, R2\nDIV_RET CALL MD_RET\n        RET\n")

cnt = 0
for i in mapa:
    f.write(i + " DW 0\n")
    cnt+=1
cnt = 0
for i in mapaPetlja:
    f.write(i + " DW 0\n")
    cnt += 1