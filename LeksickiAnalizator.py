import fileinput

cnt = 0

for line in fileinput.input():
    cnt += 1

    if line.startswith("//"):
        continue

    elif line[0].isdigit() or line[0] == "("  or line[0] == ")" or line[0] == "+" or line[0] == "-" or line[0] == "/" or line[0] == "*" or line[0] == " ":
        i = 0
        k = 0
        while line[k] == " ":
            i += 1
            k += 1

        while line[i] != ("\n"):

            if (line[i] == "/" and line[i + 1] == "/") or (line[i] == "/" and line[i - 1] == "/"):
                break

            elif line[i] == "=":
                print("OP_PRIDRUZI " + str(cnt) + " " + line[i])

            elif line[i] == "z" and line[i + 1] == "a" and not line[i + 2].isalnum():
                print("KR_ZA " + str(cnt) + " za")
                i += 1
            elif line[i] == "o" and line[i + 1] == "d" and not line[i + 2].isalnum():
                print("KR_OD " + str(cnt) + " od")
                i += 1
            elif line[i] == "d" and line[i + 1] == "o" and not line[i + 2].isalnum():
                print("KR_DO " + str(cnt) + " do")
                i += 1
            elif line[i] == "a" and line[i + 1] == "z" and not line[i + 2].isalnum():
                print("KR_AZ " + str(cnt) + " az")
                i += 1

            elif line[i].isalpha():
                j = i
                idn2 = []

                while line[j].isalnum():
                    idn2.append(line[j])
                    j += 1
                print("IDN " + str(cnt) + " " + ''.join(idn2))
                j -= 1
                i = j

            elif line[i].isdigit():
                j = i
                idn2 = []

                while line[j].isdigit():
                    idn2.append(line[j])
                    j += 1
                print("BROJ " + str(cnt) + " " + ''.join(str(e) for e in idn2))
                j -= 1
                i = j

            elif line[i] == "+":
                print("OP_PLUS " + str(cnt) + " " + "+")
            elif line[i] == "-":
                print("OP_MINUS " + str(cnt) + " " + "-")
            elif line[i] == "*":
                print("OP_PUTA " + str(cnt) + " " + "*")
            elif line[i] == "/":
                print("OP_DIJELI " + str(cnt) + " " + "/")
            elif line[i] == "(":
                print("L_ZAGRADA " + str(cnt) + " " + "(")
            elif line[i] == ")":
                print("D_ZAGRADA " + str(cnt) + " " + ")")

            i += 1

    elif line[0].isalpha():

        i = 0
        k = 0
        while line[k] == " ":
            i += 1
            k += 1

        while line[i] != ("\n"):


            if (line[i] == "/" and line[i+1] == "/") or (line[i] == "/" and line[i-1] == "/"):
                break

            elif line[i] == "z" and line[i + 1] == "a" and not line[i+2].isalnum():
                print("KR_ZA " + str(cnt) + " za")
                i += 1
            elif line[i] == "o" and line[i + 1] == "d" and not line[i+2].isalnum():
                print("KR_OD " + str(cnt) + " od")
                i += 1
            elif line[i] == "d" and line[i + 1] == "o" and not line[i+2].isalnum():
                print("KR_DO " + str(cnt) + " do")
                i += 1
            elif line[i] == "a" and line[i+1] == "z" and not line[i+2].isalnum():
                print("KR_AZ " + str(cnt) + " az")
                i += 1

            elif line[i] == "=":
                print("OP_PRIDRUZI " + str(cnt) + " " + line[i])

            elif line[i].isalpha():
                j = i
                idn2 = []

                while line[j].isalnum():
                    idn2.append(line[j])
                    j += 1
                print("IDN " + str(cnt) + " " + ''.join(idn2))
                j -= 1
                i = j

            elif line[i].isdigit():
                j = i
                idn2 = []

                while line[j].isdigit():
                    idn2.append(line[j])
                    j += 1
                print("BROJ " + str(cnt) + " " + ''.join(str(e) for e in idn2))
                j -= 1
                i = j

            elif line[i] == "+":
                print("OP_PLUS " + str(cnt) + " " + "+")
            elif line[i] == "-":
                print("OP_MINUS " + str(cnt) + " " + "-")
            elif line[i] == "*":
                print("OP_PUTA " + str(cnt) + " " + "*")
            elif line[i] == "/":
                print("OP_DIJELI " + str(cnt) + " " + "/")
            elif line[i] == "(":
                print("L_ZAGRADA " + str(cnt) + " " + "(")
            elif line[i] == ")":
                print("D_ZAGRADA " + str(cnt) + " " + ")")

            i += 1