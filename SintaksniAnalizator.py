import fileinput

# def za_petlja(line, cnt):
#     if line.startswith("KR_ZA"):
#         cnt += 1
#         stog.pop()
#         stog.append("E")
#         stog.append("E")
#         stog.append("LN")
#         for i in range(0, cnt):
#             text += " "
#         text += line
#         stablo.append(text)
#     elif line.startswith("IDN"):
#         for i in range(0, cnt):
#             text += " "
#         text += line
#         stablo.append(text)
#     elif line.startswith("KR_OD"):
#         for i in range(0, cnt):
#             text += " "
#         text += line
#         stablo.append(text)
#     return cnt


# def naredba_pridruzivanja(line, cnt):
#     cnt += 1
#     if line.startswith("IDN"):
#         stog.pop()
#         stog.append("E")
#         for i in range(0, cnt):
#             text += " "
#         text += line
#         stablo.append(text)
#         lista = line.split(" ")
#         stablo.append("OP_PRIDRUZI" + " " + lista[1] + " " + "=")
#     return cnt


# def naredba(line, cnt):
#     cnt += 1
#     if line.startswith("IDN"):
#         stog.pop()
#         stog.append("naredba_pridruzivanja")
#         for i in range(0, cnt):
#             text += " "
#         text += "<naredba_pridruzivanja>"
#         stablo.append(text)
#         cnt = naredba_pridruzivanja(line. cnt)
#     elif line.startswith("KR_ZA"):
#         stog.pop()
#         stog.append("za_petlja")
#         for i in range(0, cnt):
#             text += " "
#         text += "<za_petlja>"
#         stablo.append(text)
#         cnt = za_petlja(line, cnt)
#     return cnt


# def lista_naredbi(line, cnt):
#     cnt += 1
#     if line.startswith("IDN") or line.startswith("KR_ZA"):
#         stog.pop()
#         stog.append("lista_naredbi")
#         stog.append("naredba")
#         text = ""
#         for i in range(0, cnt):
#             text += " "
#         text += "<naredba>"
#         stablo.append(text)
#         cnt = naredba(line, cnt)
#     elif line.startswith("KR_AZ") or line.startswith(""):
#         stog.pop()
#     return cnt


# def program(line, cnt):
#     cnt += 1
#     if line.startswith("IDN") or line.startswith("") or line.startswith("KR_ZA"):
#         stog.pop()
#         stog.append("lista_naredbi")
#         text = ""
#         for i in range(0, cnt):
#             text += " "
#         text += "<lista_naredbi>"
#         stablo.append(text)
#         cnt = lista_naredbi(line, cnt)
#     return cnt


stog = ["program"]
stablo = ["<program>"]
cnt = 1

for line in fileinput.input():
    flag = True
    line = line.rstrip()
    while flag:
        if stog[len(stog) - 1] == "program":
            if line.startswith("IDN") or line.startswith("prazno") or line.startswith("KR_ZA"):
                stog.pop()
                stog.append(cnt)
                stog.append("lista_naredbi")
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "lista_naredbi":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<lista_naredbi>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("KR_ZA"):
                cnt += 1
                stog.append(cnt)
                stog.append("lista_naredbi")
                stog.append(cnt)
                stog.append("naredba")
            elif line.startswith("KR_AZ") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "naredba":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<naredba>"
            stablo.append(text)
            if line.startswith("IDN"):
                cnt += 1
                stog.append(cnt)
                stog.append("naredba_pridruzivanja")
            elif line.startswith("KR_ZA"):
                cnt += 1
                stog.append(cnt)
                stog.append("za_petlja")
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "naredba_pridruzivanja":
            if line.startswith("IDN"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "<naredba_pridruzivanja>"
                stablo.append(text)
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                stog.append(cnt)
                stog.append("E")
                stog.append(cnt)
                stog.append("OP_PRIDRUZI")
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "za_petlja":
            if line.startswith("KR_ZA"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "<za_petlja>"
                stablo.append(text)
                cnt += 1
                stog.append(cnt)
                stog.append("KR_AZ")
                stog.append(cnt)
                stog.append("lista_naredbi")
                stog.append(cnt)
                stog.append("E")
                stog.append(cnt)
                stog.append("KR_DO")
                stog.append(cnt)
                stog.append("E")
                stog.append(cnt)
                stog.append("KR_OD")
                stog.append(cnt)
                stog.append("IDN")
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "E":
            if line.startswith("IDN") or line.startswith("OP_PLUS") or line.startswith("OP_MINUS")or line.startswith("L_ZAGRADA") or line.startswith("BROJ"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "<E>"
                stablo.append(text)
                cnt += 1
                stog.append(cnt)
                stog.append("E_lista")
                stog.append(cnt)
                stog.append("T")
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "E_lista":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<E_lista>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("KR_ZA") or line.startswith("KR_DO") or line.startswith("KR_AZ") or line.startswith("D_ZAGRADA") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            elif line.startswith("OP_PLUS") or line.startswith("OP_MINUS"):
                cnt += 1
                stog.append(cnt)
                stog.append("E")
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "T":
            if line.startswith("IDN") or line.startswith("OP_PLUS") or line.startswith("OP_MINUS") or line.startswith("L_ZAGRADA") or line.startswith("BROJ"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "<T>"
                stablo.append(text)
                cnt += 1
                stog.append(cnt)
                stog.append("T_lista")
                stog.append(cnt)
                stog.append("P")
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "T_lista":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<T_lista>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("KR_ZA") or line.startswith("KR_DO") or line.startswith("KR_AZ") or line.startswith("OP_PLUS") or line.startswith("OP_MINUS") or line.startswith("D_ZAGRADA") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            elif line.startswith("OP_PUTA") or line.startswith("OP_DIJELI"):
                cnt += 1
                stog.append(cnt)
                stog.append("T")
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "P":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<P>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("BROJ"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            elif line.startswith("OP_PLUS") or line.startswith("OP_MINUS"):
                cnt += 1
                stog.append(cnt)
                stog.append("P")
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            elif line.startswith("L_ZAGRADA"):
                cnt += 1
                stog.append(cnt)
                stog.append("D_ZAGRADA")
                stog.append(cnt)
                stog.append("E")
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "OP_PRIDRUZI":
            if line.startswith("OP_PRIDRUZI"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "IDN":
            if line.startswith("IDN"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "KR_OD":
            if line.startswith("KR_OD"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "KR_DO":
            if line.startswith("KR_DO"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "KR_AZ":
            if line.startswith("KR_AZ"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "D_ZAGRADA":
            if line.startswith("D_ZAGRADA"):
                stog.pop()
                cnt = stog.pop()
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += line
                stablo.append(text)
                flag = False
            else:
                print("err " + line)
                exit()

line = "prazno"

if len(stog) != 0:
    while(len(stog) != 0):
        if stog[len(stog) - 1] == "program":
            if line.startswith("IDN") or line.startswith("prazno") or line.startswith("KR_ZA"):
                stog.pop()
                stog.append(cnt)
                stog.append("lista_naredbi")
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "lista_naredbi":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<lista_naredbi>"
            stablo.append(text)
            if line.startswith("KR_AZ") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "E_lista":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<E_lista>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("KR_ZA") or line.startswith("KR_DO") or line.startswith("KR_AZ") or line.startswith("D_ZAGRADA") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            else:
                print("err " + line)
                exit()
        elif stog[len(stog) - 1] == "T_lista":
            stog.pop()
            cnt = stog.pop()
            text = ""
            for i in range(0, cnt):
                text += " "
            text += "<T_lista>"
            stablo.append(text)
            if line.startswith("IDN") or line.startswith("KR_ZA") or line.startswith("KR_DO") or line.startswith("KR_AZ") or line.startswith("OP_PLUS") or line.startswith("OP_MINUS") or line.startswith("D_ZAGRADA") or line.startswith("prazno"):
                cnt += 1
                text = ""
                for i in range(0, cnt):
                    text += " "
                text += "$"
                stablo.append(text)
            else:
                print("err " + line)
                exit()
        else:
            print("err kraj")
            exit()

for i in stablo:
    print(i)